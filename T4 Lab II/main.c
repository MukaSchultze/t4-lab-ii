#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include "grafo.h"
#include "lista.h"
#include "io.h"

/*
* Samuel Schultze - SI - 07/07/2018 - Lab II
* gcc *.c -Wall -Wextra
*/

int main() {

	PrintLine("Insira os vertices do grafo: ");

	Lista* listaAdj = NULL;

	listaAdj = GrafoLeInput(listaAdj);

	/*listaAdj = ListaPush(listaAdj, GrafoAlocaVertice(1));
	listaAdj = ListaPush(listaAdj, GrafoAlocaVertice(2));
	listaAdj = ListaPush(listaAdj, GrafoAlocaVertice(3));
	listaAdj = ListaPush(listaAdj, GrafoAlocaVertice(4));
	listaAdj = ListaPush(listaAdj, GrafoAlocaVertice(5));

	GrafoAdicionaAdjacencia(listaAdj, listaAdj->grafo, 3);
	GrafoAdicionaAdjacencia(listaAdj, listaAdj->grafo, 2);
	GrafoAdicionaAdjacencia(listaAdj, listaAdj->grafo, 5);
	GrafoAdicionaAdjacencia(listaAdj, listaAdj->prox->grafo, 5);
	GrafoAdicionaAdjacencia(listaAdj, listaAdj->prox->grafo, 4);
	GrafoAdicionaAdjacencia(listaAdj, listaAdj->prox->grafo, 3);
	GrafoAdicionaAdjacencia(listaAdj, listaAdj->prox->prox->grafo, 1);
	GrafoAdicionaAdjacencia(listaAdj, listaAdj->prox->prox->prox->grafo, 5);
	GrafoAdicionaAdjacencia(listaAdj, listaAdj->prox->prox->prox->grafo, 3);*/

	while (true) {

		PrintLine("");
		PrintLine("");
		PrintLine("0 - Sair");
		PrintLine("1 - Imprimir grafo");
		PrintLine("2 - Percorrer - Profundidade - Pre ordem");
		PrintLine("3 - Percorrer - Profundidade - Pos ordem");
		PrintLine("4 - Percorrer - Largura - Pre ordem");
		PrintLine("5 - Percorrer - Largura - Pos ordem");
		PrintLine("");

		int opt = ReadInt("Insira a opcao: ");

		if (opt == 0)
			return 0;

		if (listaAdj == NULL) {
			printf("\nGrafo vazio");
			continue;
		}

		int	value = ReadInt("Valor do vertice inicial: ");

		if (value == 0) {
			PrintLine("Valor invalido");
			continue;
		}

		GrafoResetaVisitados(listaAdj);
		Lista* g = ListaBusca(listaAdj, value);

		if (g == NULL || g->grafo == NULL) {
			PrintLine("Valor nao encontrado");
			continue;
		}

		switch (opt) {

			case 1:
				for (Lista* l = listaAdj; l != NULL; l = l->prox) {
					printf("\nVertice %d adjacente a ", l->grafo->valor);

					if (l->grafo->adjacentes == NULL)
						printf("nenhuma vertice");

					for (Lista* adj = l->grafo->adjacentes; adj != NULL; adj = adj->prox) {
						printf("%d", adj->grafo->valor);

						if (adj->prox != NULL)
							printf(", ");
					};
				}
				break;

			case 2:
				printf("\nProfundidade - Pre ordem: ");
				GrafoImprimePreProf(g->grafo);
				break;

			case 3:
				printf("\nProfundidade - Pos ordem: ");
				GrafoImprimePosProf(g->grafo);
				break;

			case 4:
				printf("\nLargura - Pre ordem: ");
				GrafoImprimePreLarg(g->grafo);
				break;

			case 5:
				printf("\nLargura - Pos ordem: ");
				GrafoImprimePosLarg(g->grafo);
				break;

		}


	}
}
