#pragma once
#include "grafo.h"

typedef struct lista {
	struct grafo* grafo;
	struct lista* prox;
} Lista;

Lista* ListaPush(Lista* lista, Grafo* item);
Lista* ListaPop(Lista* lista, struct grafo** outValue);
Lista* ListaBusca(Lista* lista, int valor);
bool ListaContem(Lista* lista, int valor);
void ListaFree(Lista* lista);