#include <stdlib.h>
#include <stdio.h>
#include "grafo.h"
#include "lista.h"
#include "io.h"

struct lista* GrafoLeInput(struct lista* l) {

	int value;

	while ((value = ReadInt("Valor do vertice (0 para continuar): ")))
		if (ListaContem(l, value))
			PrintLine("Vertice ja adicionado");
		else
			l = ListaPush(l, GrafoAlocaVertice(value));

	for (Lista* i = l; i != NULL; i = i->prox) {
		printf("Adjacentes ao vertice %d (0 para ir para o proximo)\n", i->grafo->valor);

		while ((value = ReadInt("Nova adjacencia: ")))
			if (value == i->grafo->valor)
				PrintLine("Adjacente a si mesmo");
			else
				GrafoAdicionaAdjacencia(l, i->grafo, value);
	}

	return l;
}

void GrafoAdicionaAdjacencia(struct lista* grafo, Grafo* vertice, int valor) {
	Lista* adj = ListaBusca(grafo, valor);

	if (ListaContem(vertice->adjacentes, valor))
		PrintLine("V�rtice j� � adjacente");
	else if (adj != NULL && adj->grafo != NULL)
		vertice->adjacentes = ListaPush(vertice->adjacentes, adj->grafo);
	else
		PrintLine("V�rtice n�o encontrada");
}

Grafo* GrafoAlocaVertice(int valor) {
	Grafo* ptr = malloc(sizeof(Grafo));
	ptr->valor = valor;
	ptr->adjacentes = NULL;
	return ptr;
}

void GrafoResetaVisitados(struct lista* listaAdj) {
	for (Lista* g = listaAdj; g != NULL; g = g->prox)
		g->grafo->visitado = false;
}

void GrafoImprimePosProf(Grafo* grafo) {
	if (grafo == NULL) {
		PrintLine("Grafo vazio");
		return;
	}

	grafo->visitado = true;

	for (Lista* g = grafo->adjacentes; g != NULL; g = g->prox)
		if (!g->grafo->visitado)
			GrafoImprimePosProf(g->grafo);

	printf("%d - ", grafo->valor);
}

void GrafoImprimePreProf(Grafo* grafo) {
	if (grafo == NULL) {
		PrintLine("Grafo vazio");
		return;
	}

	printf("%d - ", grafo->valor);
	grafo->visitado = true;

	for (Lista* g = grafo->adjacentes; g != NULL; g = g->prox)
		if (!g->grafo->visitado)
			GrafoImprimePreProf(g->grafo);
}

void GrafoImprimePosLarg(Grafo* grafo) {
	if (grafo == NULL) {
		PrintLine("Grafo vazio");
		return;
	}

	grafo->visitado = true;
	Lista* fila = NULL;
	fila = ListaPush(fila, grafo);

	while (fila != NULL) {
		Grafo*  u;
		fila = ListaPop(fila, &u);
		struct lista * adj = u->adjacentes;
		for (struct lista* i = adj; i != NULL; i = i->prox)
			if (!i->grafo->visitado) {
				i->grafo->visitado = true;
				fila = ListaPush(fila, i->grafo);
			}

		printf("%d - ", u->valor);
	}
}

void GrafoImprimePreLarg(Grafo* grafo) {
	if (grafo == NULL) {
		PrintLine("Grafo vazio");
		return;
	}

	grafo->visitado = true;
	printf("%d - ", grafo->valor);
	Lista* fila = NULL;
	fila = ListaPush(fila, grafo);

	while (fila != NULL) {
		Grafo*  u;
		fila = ListaPop(fila, &u);
		struct lista * adj = u->adjacentes;
		for (struct lista* i = adj; i != NULL; i = i->prox)
			if (!i->grafo->visitado) {
				i->grafo->visitado = true;
				printf("%d - ", i->grafo->valor);
				fila = ListaPush(fila, i->grafo);
			}
	}
}