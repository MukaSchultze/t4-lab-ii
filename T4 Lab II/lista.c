#include <stdlib.h>
#include "lista.h"

Lista* ListaPush(Lista* lista, Grafo* item) {
	Lista* ptr = malloc(sizeof(Lista));

	ptr->grafo = item;
	ptr->prox = NULL;

	if (lista == NULL)
		return ptr;

	for (Lista* l = lista; l != NULL; l = l->prox)
		if (l->prox == NULL) {
			l->prox = ptr;
			break;
		}

	return lista;
}

Lista* ListaPop(Lista* lista, struct grafo** outValue) {
	if (lista == NULL)
		return NULL;

	*outValue = lista->grafo;

	Lista* result = lista->prox;
	free(lista);
	return result;
}

Lista* ListaBusca(Lista* lista, int valor) {
	for (Lista* l = lista; l != NULL; l = l->prox)
		if (l->grafo->valor == valor)
			return l;

	return NULL;
}

bool ListaContem(Lista* lista, int valor) {
	return ListaBusca(lista, valor) != NULL;
}

void ListaFree(Lista* lista) {
	if (lista == NULL)
		return;

	ListaFree(lista->prox);
	free(lista);
}
