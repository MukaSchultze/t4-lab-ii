#pragma once
#include <stdbool.h> 

typedef struct grafo {
	int valor;
	bool visitado;
	struct lista* adjacentes;
} Grafo;

struct lista* GrafoLeInput(struct lista* l);

void GrafoAdicionaAdjacencia(struct lista* grafo, Grafo* vertice, int valor);

Grafo* GrafoAlocaVertice(int valor);

void GrafoResetaVisitados(struct lista* listaAdj);

void GrafoImprimePosProf(Grafo* grafo);
void GrafoImprimePreProf(Grafo* grafo);

void GrafoImprimePosLarg(Grafo* grafo);
void GrafoImprimePreLarg(Grafo* grafo);