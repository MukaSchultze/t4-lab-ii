#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include "io.h"

void PrintLine(char* str) {
	printf("%s\n", str);
}

int ReadInt(char* label) {
	int result;
	printf("%s", label);
	scanf("%d", &result);
	return result;
}